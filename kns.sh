#!/bin/bash 
if [ $1 == "-h" ] || [$1 == "--help" ]; then
    echo "Usage: $0 [namespace]"
    echo "list kubernetes's namespaces available or change the current one active"
fi

[ $(command -v kubectl) ] || (echo "kubectl program is not present" && exit 1)


if (( $# == 1 )); then
    kubectl config set-context --current --namespace=$1
    echo "Current namespace changed to $1"
else
    namespace=$(kubectl config view --minify --output 'jsonpath={..namespace}')
    kubectl get ns -o name
    echo "Current namespace is: $namespace"
fi 
